The PnP Semi-Automatic Feeder (https://bitbucket.org/Loonatec/pnp_semi-automatic_feeder/ or https://loonatec.com/pnp-semi-automatic-feeder/)
by Balloon Ascent Technologies LLC (https://Loonatec.com)

This product has been designed by Balloon Ascent Technologies LLC, based on
prior work by Dining-Philosopher (https://github.com/dining-philosopher/litefeeder).
It has been licensed under the CERN Open Hardware License v1.2 (see included License.txt file).
All trademarks (Loonatec) are owned by the designer, Balloon Ascent Technologies LLC.
Please respect the trademarks of others.

# Summary
This is a semi-automatic printable SMD feeder for both manual and automatic pick-and-place (PnP) machines.
It uses the mechanical energy from a level depressed by the user or picking head to both advance the carrier tape,
and to peel off and take-up the cover tape.

# 3D Printing Notes
This has been designed for a 0.4 mm FDM nozzle with 0.4 mm print widths and 0.2 mm layer heights. One of the
FreeCAD model parameters is a "Printer Fudge Factor" to account for slicer and printer differences. Use it to
ensure tight fits. Expected value range for this parameter is +0.2 to -0.2 mm.

The Spool prints better in vase mode above 0.8 mm heights. It removes any seam artifacts that cause the inside
surface to not be smooth and slide well.

# Change-log
Initial release 2020-02-08
FreeCAD & ratching sprocket change 2020-10-23
